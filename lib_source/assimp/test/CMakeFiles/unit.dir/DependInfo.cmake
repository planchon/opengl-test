# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/paul/OPENGL/lib_source/assimp/test/unit/CCompilerTest.c" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/CCompilerTest.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ASSIMP_BUILD_NO_C4D_IMPORTER"
  "ASSIMP_BUILD_NO_OWN_ZLIB"
  "ASSIMP_TEST_MODELS_DIR=\"/home/paul/OPENGL/lib_source/assimp/test/models\""
  "OPENDDL_NO_USE_CPP11"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "include"
  "."
  "test/../contrib/gtest/include"
  "test/../contrib/gtest"
  "code"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/paul/OPENGL/lib_source/assimp/code/Version.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/__/code/Version.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/contrib/gtest/src/gtest-all.cc" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/__/contrib/gtest/src/gtest-all.cc.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/AbstractImportExportBase.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/AbstractImportExportBase.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/AssimpAPITest.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/AssimpAPITest.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/Main.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/Main.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/SceneDiffer.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/SceneDiffer.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/ut3DImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/ut3DImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/ut3DSImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/ut3DSImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utACImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utACImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utAMFImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utAMFImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utASEImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utASEImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utAnim.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utAnim.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utB3DImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utB3DImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utBVHImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utBVHImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utBatchLoader.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utBatchLoader.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utBlendImportAreaLight.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utBlendImportAreaLight.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utBlendImportMaterials.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utBlendImportMaterials.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utBlenderImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utBlenderImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utBlenderIntermediate.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utBlenderIntermediate.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utCSMImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utCSMImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utColladaExportCamera.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utColladaExportCamera.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utColladaExportLight.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utColladaExportLight.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utColladaImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utColladaImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utD3MFImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utD3MFImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utDXFImporterExporter.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utDXFImporterExporter.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utDefaultIOStream.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utDefaultIOStream.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utFBXImporterExporter.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utFBXImporterExporter.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utFastAtof.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utFastAtof.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utFindDegenerates.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utFindDegenerates.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utFindInvalidData.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utFindInvalidData.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utFixInfacingNormals.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utFixInfacingNormals.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utGenNormals.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utGenNormals.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utHMPImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utHMPImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utIFCImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utIFCImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utIOStreamBuffer.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utIOStreamBuffer.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utIOSystem.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utIOSystem.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utImporter.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utImporter.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utImproveCacheLocality.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utImproveCacheLocality.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utIssues.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utIssues.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utJoinVertices.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utJoinVertices.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utLWSImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utLWSImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utLimitBoneWeights.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utLimitBoneWeights.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utMaterialSystem.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utMaterialSystem.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utMatrix3x3.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utMatrix3x3.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utMatrix4x4.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utMatrix4x4.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utMetadata.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utMetadata.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utObjImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utObjImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utObjTools.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utObjTools.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utOpenGEXImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utOpenGEXImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utPLYImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utPLYImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utPMXImporter.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utPMXImporter.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utPretransformVertices.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utPretransformVertices.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utProfiler.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utProfiler.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utQ3DImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utQ3DImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utRemoveComments.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utRemoveComments.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utRemoveComponent.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utRemoveComponent.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utRemoveRedundantMaterials.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utRemoveRedundantMaterials.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utRemoveVCProcess.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utRemoveVCProcess.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utSIBImporter.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utSIBImporter.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utSMDImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utSMDImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utSTLImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utSTLImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utScaleProcess.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utScaleProcess.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utSceneCombiner.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utSceneCombiner.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utScenePreprocessor.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utScenePreprocessor.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utSharedPPData.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utSharedPPData.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utSortByPType.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utSortByPType.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utSplitLargeMeshes.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utSplitLargeMeshes.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utStringUtils.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utStringUtils.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utTargetAnimation.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utTargetAnimation.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utTextureTransform.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utTextureTransform.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utTriangulate.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utTriangulate.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utTypes.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utTypes.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utVector3.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utVector3.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utVersion.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utVersion.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utVertexTriangleAdjacency.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utVertexTriangleAdjacency.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utX3DImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utX3DImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utXImporterExporter.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utXImporterExporter.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utglTF2ImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utglTF2ImportExport.cpp.o"
  "/home/paul/OPENGL/lib_source/assimp/test/unit/utglTFImportExport.cpp" "/home/paul/OPENGL/lib_source/assimp/test/CMakeFiles/unit.dir/unit/utglTFImportExport.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ASSIMP_BUILD_NO_C4D_IMPORTER"
  "ASSIMP_BUILD_NO_OWN_ZLIB"
  "ASSIMP_TEST_MODELS_DIR=\"/home/paul/OPENGL/lib_source/assimp/test/models\""
  "OPENDDL_NO_USE_CPP11"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "include"
  "."
  "test/../contrib/gtest/include"
  "test/../contrib/gtest"
  "code"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/paul/OPENGL/lib_source/assimp/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  "/home/paul/OPENGL/lib_source/assimp/contrib/irrXML/CMakeFiles/IrrXML.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
