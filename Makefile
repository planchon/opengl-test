FLAG =  -lglfw3 -lassimp -lGL -lX11 -lpthread -lXrandr -lXi -ldl -lXxf86vm -lXinerama -lXcursor
CPP  = g++ -std=c++17
OUT  = game

all:
	g++ -o bin/game -std=c++17 src/glad.c src/mesh.cpp src/camera.cpp src/shader.cpp src/main.cpp $(FLAG)
