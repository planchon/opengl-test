#include "../include/glm/glm.hpp"
#include "../include/glm/gtc/matrix_transform.hpp"
#include "../include/glm/gtc/type_ptr.hpp"

#include <GLFW/glfw3.h>
#include <iostream>

class Camera {
public:
  glm::vec3 pos;
  glm::vec3 front;
  glm::vec3 up;

  double lastMouseX = 400;
  double lastMouseY = 300;
  double mouseYaw   = 0;
  double mousePitch = 0;
  double fov        = 45;
  double maxFOV     = 110;
  bool   firstMouse = true;

  float near = 0.1f;
  float far  = 100.0f;
  
  float cameraMouvementSpeed = 0.08;
  float cameraMouseSpeed     = 0.1;
  
  Camera(const glm::vec3 start_pos);
  
  void updatePosition(GLFWwindow* window);
  void updateMouse(double xpos, double ypos);
  void updateZoom(double yoffset);
  glm::mat4 getViewMatrice();
  float getFOV();
};

