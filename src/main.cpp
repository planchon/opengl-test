#define STB_IMAGE_IMPLEMENTATION

#include "../include/glad/glad.h"
#include "../include/stb/stb_image.h"

// pour les maths
#include "../include/glm/glm.hpp"
#include "../include/glm/gtc/matrix_transform.hpp"
#include "../include/glm/gtc/type_ptr.hpp"

// pour les import
#include "../include/assimp/Importer.hpp"

#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <iterator>

#include "shader.hpp"
#include "camera.hpp"

const unsigned int SCREEN_HEIGHT = 600;
const unsigned int SCREEN_WIDTH  = 800;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput (GLFWwindow* window);
void clearScreen();

float vertices[] = {
  // positions          // normals           // texture
  -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
   0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
   0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
   0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
  -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
  -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

  -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
   0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
   0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
   0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
  -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
  -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

  -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
  -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
  -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
  -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
  -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
  -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

   0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
   0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
   0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
   0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
   0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
   0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

  -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
   0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
   0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
   0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
  -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
  -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

  -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
   0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
   0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
   0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
  -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
  -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
};

glm::vec3 positions[] = {
  glm::vec3( 0.0f,  0.0f,  0.0f),
  glm::vec3( 2.0f,  5.0f, -15.0f),
  glm::vec3(-1.5f, -2.2f, -2.5f),
  glm::vec3(-3.8f, -2.0f, -12.3f),
  glm::vec3( 2.4f, -0.4f, -3.5f),
  glm::vec3(-1.7f,  3.0f, -7.5f),
  glm::vec3( 1.3f, -2.0f, -2.5f),
  glm::vec3( 1.5f,  2.0f, -2.5f),
  glm::vec3( 1.5f,  0.2f, -1.5f),
  glm::vec3(-1.3f,  1.0f, -1.5f)
};

glm::vec3 lightPos = glm::vec3(5.0f, 0.0f, 1.0f);

// camera global plus simple
Camera camera(glm::vec3(0,0,5.0f));

// resize la fenetre des que l'user la resize
void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
  glViewport(0, 0, width, height);
}

void processInput (GLFWwindow* window) {
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);

  camera.updatePosition(window);
 }

void clearScreen() {
  glClearColor(0, 0, 0, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
  camera.updateMouse(xpos, ypos);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
  camera.updateZoom(yoffset);
}

unsigned int load_image(char const* path) {
  unsigned int textureID;
  glGenTextures(1, &textureID);

  int w, h, nComp;
  unsigned char* data = stbi_load(path, &w, &h, &nComp, 0);
  if (data) {
    GLenum format;
    if (nComp == 1)
      format = GL_RED;
    if (nComp == 3)
      format = GL_RGB;
    if (nComp == 4)
      format = GL_RGBA;

    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    stbi_image_free(data);
  } else {
     std::cout << "ERROR::TEXTURE::Fail_to_open_texture" << std::endl;
     stbi_image_free(data);
  }

  return textureID;
}

int main() {

  // INITIALISATION DE OPENGL
  
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Test OPENGL", NULL, NULL);
  if (window == NULL) {
    std::cout << "Error in the creation of the window" << std::endl;
    glfwTerminate();
    return -1;
  } else {
    std::cout << "Window created." << std::endl;
  }

  glfwMakeContextCurrent(window);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback); 
  
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    std::cout << "Error in the initialisation of GLAD" << std::endl;
    return -1;
  } else {
    std::cout << "GLAD initialized." << std::endl;
  }

  glEnable(GL_DEPTH_TEST);

  // INITIALISTION DE OPEN GL
  
  Shader world("src/vertex_world.glsl", "src/fragment_world.glsl");
  Shader light("src/vertex_light.glsl", "src/fragment_light.glsl");

  // partie des 10 cubes
  
  unsigned int VBO, cubeVAO; // vertex buffer object (une liste de vertex, pour que la CG soit rapide de ouf).
  glGenVertexArrays(1, &cubeVAO);
  glGenBuffers(1, &VBO);

  glBindVertexArray(cubeVAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  // position
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  
  // normal
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);

  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
  glEnableVertexAttribArray(2);

  // fin partie des 10 cubes

  // partie de la lumiere

  unsigned int lightVAO;
  glGenVertexArrays(1, &lightVAO);
  glBindVertexArray(lightVAO);
  
  glBindBuffer(GL_ARRAY_BUFFER, VBO);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  // fin de la partie lumiere

  // creation de la texture du cube
  unsigned int diffuseMap  = load_image("res/cube_diffuse.jpg");
  unsigned int specularMap = load_image("res/cube_specular.jpg");

  world.use();
  world.setInt("material.diffuse", 0);
  world.setInt("material.specular", 1);
  
  // constante pour la game engine
  int maxTicks = 60;
  double limitTicks = 1.0f / (float) maxTicks;
  double lastTime = glfwGetTime(), timer = lastTime;
  double deltaTime = 0, nowTime = 0;
  int frames = 0, ticks = 0;
    
  // boucle principale
  while(!glfwWindowShouldClose(window)) {

    // boucle logique TODO : faire des fonction tick et render
    nowTime = glfwGetTime();
    deltaTime += (nowTime - lastTime) / limitTicks;
    lastTime = nowTime;

    if (deltaTime > 1) {
      processInput(window);
      ticks++;
      deltaTime--;
    }

    if (glfwGetTime() - timer > 1.0) {
      timer++;
      std::cout << "fps: " << frames << ", ticks: " << ticks << std::endl;
      frames = 0;
      ticks  = 0;
    }

    frames++;

    // fin boucle logique
    // boucle render
    
    clearScreen();

    glm::mat4 projection_mat = glm::mat4(1.0f);
    projection_mat = glm::perspective(glm::radians(camera.getFOV()), (float) SCREEN_WIDTH / (float) SCREEN_HEIGHT, camera.near, camera.far);
    
    // // rendu des cube et de leur texture
    world.use();
    world.setVec3("light.position", lightPos);
    world.setVec3("viewPos", camera.pos);

    world.setVec3("light.ambient", 0.2f, 0.2f, 0.2f);
    world.setVec3("light.diffuse", 0.5f, 0.5f, 0.5f);
    world.setVec3("light.specular", 1.0f, 1.0f, 1.0f);

    world.setVec3("material.specular", 0.5f, 0.5f, 0.5f);
    world.setFloat("material.shininess", 128.0f);
    
    world.setMat4("view", camera.getViewMatrice());
    world.setMat4("projection", projection_mat);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, diffuseMap);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, specularMap);
    
    for (unsigned int i = 0; i < std::size(positions); i++) {
      glm::mat4 model_mat = glm::mat4(1.0f);
      model_mat = glm::translate(model_mat, positions[i]);
      world.setMat4("model", model_mat);

      glBindVertexArray(cubeVAO);
      glDrawArrays(GL_TRIANGLES, 0, 36);
    }

    // fin du rendu des cubes

    light.use();
    light.setMat4("projection", projection_mat);
    light.setMat4("view", camera.getViewMatrice());

    glm::mat4 model_mat = glm::mat4(1.0f);
    model_mat = glm::translate(model_mat, lightPos);
    model_mat = glm::scale(model_mat, glm::vec3(0.2f, 0.2f, 0.2f));
    light.setMat4("model", model_mat);

    glBindVertexArray(lightVAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
        
    glfwSwapBuffers(window);
    glfwPollEvents();    
  }

  glDeleteVertexArrays(1, &cubeVAO);
  glDeleteVertexArrays(1, &lightVAO);
  glDeleteBuffers(1, &VBO);

  glfwTerminate();
  return 0;
}
