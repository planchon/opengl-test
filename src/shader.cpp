#include "shader.hpp"

Shader::Shader(const GLchar* vertexShaderPath, const GLchar* fragmentShaderPath) {
  std::string vertexShaderCodeString;
  std::string fragmentShaderCodeString;
  std::ifstream vertexShaderFile;
  std::ifstream fragmentShaderFile;

  vertexShaderFile.exceptions   (std::ifstream::failbit | std::ifstream::badbit);
  fragmentShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);

  try {
    vertexShaderFile.open(vertexShaderPath);
    fragmentShaderFile.open(fragmentShaderPath);
    
    std::stringstream vertexShaderStream, fragmentShaderStream;
    
    vertexShaderStream << vertexShaderFile.rdbuf();
    fragmentShaderStream << fragmentShaderFile.rdbuf();
    
    vertexShaderFile.close();
    fragmentShaderFile.close();
      
    vertexShaderCodeString   = vertexShaderStream.str();
    fragmentShaderCodeString = fragmentShaderStream.str();
  } catch (std::ifstream::failure e) {
    std::cout << "ERROR::SHADER::READ_FAILED : " << e.what() << std::endl;
  }

  const char* vertexShaderCode   = vertexShaderCodeString.c_str();
  const char* fragmentShaderCode = fragmentShaderCodeString.c_str();

  unsigned int vertexShader, fragmentShader;
  int success;
  char infoLog[512];

  // vertexShader compilation
  vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexShaderCode, NULL);
  glCompileShader(vertexShader);

  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
  
  if (!success) {
    glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
    std::cout << "ERROR::SHADER::VERTEX_COMPILATION_FAILED\n" << infoLog << std::endl;
  }

  fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentShaderCode, NULL);
  glCompileShader(fragmentShader);

  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
  
  if (!success) {
    glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
    std::cout << "ERROR::SHADER::FRAGMENT_COMPILATION_FAILED\n" << infoLog << std::endl;
  }

  ID = glCreateProgram();
  glAttachShader(ID, vertexShader);
  glAttachShader(ID, fragmentShader);
  glLinkProgram(ID);

  glGetProgramiv(ID, GL_LINK_STATUS, &success);

  if (!success) {
    glGetProgramInfoLog(ID, 512, NULL, infoLog);
    std::cout << "ERROR::SHADER::PROGRAM_LINKED_FAILED\n" << infoLog << std::endl;
  }

  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
  
}

void Shader::use() {
  glUseProgram(ID);
}

void Shader::setBool(const std::string &varName, bool value) const {
  glUniform1i(glGetUniformLocation(ID, varName.c_str()), (int)value);
}

void Shader::setInt(const std::string &varName, int value) const {
  glUniform1i(glGetUniformLocation(ID, varName.c_str()), value);
}

void Shader::setFloat(const std::string &varName, float value) const {
  glUniform1f(glGetUniformLocation(ID, varName.c_str()), value);
}

void Shader::setVec2(const std::string &varName, float x, float y) const {
  glUniform2f(glGetUniformLocation(ID, varName.c_str()), x, y);
}

void Shader::setVec2(const std::string &varName, const glm::vec2 &value) const {
  glUniform2fv(glGetUniformLocation(ID, varName.c_str()), 1, &value[0]);
}

void Shader::setVec3(const std::string &varName, float x, float y, float z) const {
  glUniform3f(glGetUniformLocation(ID, varName.c_str()), x, y, z);
}

void Shader::setVec3(const std::string &varName, const glm::vec3 &value) const {
  glUniform3fv(glGetUniformLocation(ID, varName.c_str()), 1, &value[0]);
}

void Shader::setVec4(const std::string &varName, float x, float y, float z, float w) const {
  glUniform4f(glGetUniformLocation(ID, varName.c_str()), x, y, z, w);
}

void Shader::setVec4(const std::string &varName, const glm::vec4 &value) const {
  glUniform4fv(glGetUniformLocation(ID, varName.c_str()), 1, &value[0]);
}

void Shader::setMat2(const std::string &varName, const glm::mat2 &value) const {
  glUniformMatrix2fv(glGetUniformLocation(ID, varName.c_str()), 1, GL_FALSE, &value[0][0]);
}

void Shader::setMat3(const std::string &varName, const glm::mat3 &value) const {
  glUniformMatrix3fv(glGetUniformLocation(ID, varName.c_str()), 1, GL_FALSE, &value[0][0]);
}

void Shader::setMat4(const std::string &varName, const glm::mat4 &value) const {
  glUniformMatrix4fv(glGetUniformLocation(ID, varName.c_str()), 1, GL_FALSE, &value[0][0]);
}
