#include "camera.hpp"

Camera::Camera(const glm::vec3 start_pos) {
  pos = start_pos;
  up = glm::vec3(0, 1.0f, 0);
}

void Camera::updatePosition(GLFWwindow* window) {
  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    pos += cameraMouvementSpeed * front;
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    pos -= cameraMouvementSpeed * front;
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    pos -= glm::normalize(glm::cross(front, up)) * cameraMouvementSpeed;
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    pos += glm::normalize(glm::cross(front, up)) * cameraMouvementSpeed;
}

void Camera::updateMouse(double xpos, double ypos) {
  if (firstMouse) {
    lastMouseX = xpos;
    lastMouseY = ypos;
    firstMouse = false;
  }
  
  float xoffset = xpos - lastMouseX;
  float yoffset = lastMouseY - ypos;
  lastMouseX= xpos;
  lastMouseY= ypos;

  float sensibility = 0.05;
  xoffset *= sensibility;
  yoffset *= sensibility;

  mouseYaw += xoffset;
  mousePitch += yoffset;

  if (mousePitch > 89.0f)
    mousePitch = 89.0f;
  if (mousePitch < -89.0f)
    mousePitch = -89.0f;

  glm::vec3 tmpfront;
  tmpfront.x = cos(glm::radians(mouseYaw)) * cos(glm::radians(mousePitch));
  tmpfront.y = sin(glm::radians(mousePitch));
  tmpfront.z = sin(glm::radians(mouseYaw)) * cos(glm::radians(mousePitch));
  front = glm::normalize(tmpfront);
}

glm::mat4 Camera::getViewMatrice() {
  glm::mat4 view = glm::mat4(1.0f);
  return glm::lookAt(pos, pos + front, up);
}

float Camera::getFOV() {
  return fov;
}

void Camera::updateZoom(double yoffset) {
  if (fov >= 1.0f && fov <= maxFOV)
    fov -= yoffset;
  if (fov < 1.0)
    fov = 1.0;
  if (fov > maxFOV)
    fov = maxFOV;
}
