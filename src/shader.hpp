#ifndef SHADER_H
#define SHADER_H

#include "../include/glad/glad.h"
#include "../include/glm/glm.hpp"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader {
public:
  unsigned int ID;

  Shader(const GLchar* vertexShaderPath, const GLchar* fragmentShaderPath);
  void use();

  void setBool (const std::string &varName, bool  value) const;
  void setInt  (const std::string &varName, int   value) const;
  void setFloat(const std::string &varName, float value) const;
  void setVec2 (const std::string &varName, float x, float y) const;
  void setVec2 (const std::string &varName, const glm::vec2 &value) const;
  void setVec3 (const std::string &varName, float x, float y, float z) const;
  void setVec3 (const std::string &varName, const glm::vec3 &value) const;
  void setVec4 (const std::string &varName, float x, float y, float z, float w) const;
  void setVec4 (const std::string &varName, const glm::vec4 &value) const;
  void setMat2 (const std::string &varName, const glm::mat2 &value) const;
  void setMat3 (const std::string &varName, const glm::mat3 &value) const;
  void setMat4 (const std::string &varName, const glm::mat4 &value) const;
};

#endif
