#include <iostream>
#include <vector>

#include "shader.hpp"

struct Vertex {
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec3 texCoords;
};

struct Texture {
  unsigned int id;
  std::string type;
};

class Mesh {
public:
  std::vector<Vertex> vertices;
  std::vector<unsigned int> indices;
  std::vector<Texture> textures;

  Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> texture);
  void render(Shader shader);

private:
  // vertex Array Object, vertex Buffer Object, Element buffer object
  unsigned int VAO, VBO, EBO;

  // pour creer tous les buffer et les trucs openGL
  void setupMesh();
};

  
